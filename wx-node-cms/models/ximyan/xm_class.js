'use strict';

/**
 * 模块依赖
 */
let mongoose = require('mongoose')
let Schema = mongoose.Schema

/**
 * 内容模型
 */
let ModelSchema = new Schema({
    school: {//  所属学校
        type: Schema.ObjectId,
        ref: 'School'
    },
    year: {// 入学年份
        type: String,
        required: true
    },
    serial_no: {// 班级序号
        type: String,
        required: true
    },
    code: {// 班级编码
        type: String,
        required: true
    },
    name: {//名称
        type: String,
        required: true
    },
    master: {// 班主任
        type: Schema.ObjectId,
        ref: 'Teacher'
    },
    begin_date: {//开始时间
        type: Date
    },
    end_date: {//结束时间
        type: Date
    },
    created_user: {//创建人
        type: String
    },
    created_source: {//创建来源 wx,sys
        type: String
    },
    created_time: {// 创建时间
        type: Date,
        default: Date.now
    },
    modified_time: {// 修改时间
        type: Date
    },
    status: {//有效状态1 有效 0 无效
        type: Number,
        default: 1
    }
});

/*ContentSchema.pre('save', function(next) {
    if (!this.isNew) return next();
    if (!this.title) {
        next(new Error('Invalid password'));
    } else {
        next();
    }
});*/

ModelSchema.methods = {

};

mongoose.model('Class', ModelSchema);