'use strict';

/**
 * 模块依赖
 */
let mongoose = require('mongoose');
let Schema = mongoose.Schema;

/**
 * 内容模型
 */
let ModelSchema = new Schema({
    group_class: {//  所属群组班级
        type: Schema.ObjectId,
        ref: 'GroupClass'
    },
    group_user: {// 发布用户id
        type: Schema.ObjectId,
        ref: 'GroupUser'
    },
    task_type: {// 类型 1作业 2公告通知
        type: Number,
        default: 1
    },
    title: {// 标题
        type: String
    },
    content: {//内容
        type: String,
        required: true
    },
    attachments: [// 附件信息
        {
            type: Schema.ObjectId,
            ref: 'Attachment'
        }
    ],
    feedback_type: {// 反馈类型 0 不需要 1 需要确认 2 需要回复
        type: Number,
        default: 1
    },
    begin_date: {//开始时间
        type: Date,
        default: Date.now
    },
    end_date: {//结束时间
        type: Date
    },
    created_user: {//创建人
        type: String
    },
    created_source: {//创建来源 wx,sys
        type: String
    },
    created_time: {// 创建时间
        type: Date,
        default: Date.now
    },
    modified_time: {// 修改时间
        type: Date
    },
    status: {//有效状态1 有效 0 无效
        type: Number,
        default: 1
    }
});

/*ContentSchema.pre('save', function(next) {
    if (!this.isNew) return next();
    if (!this.title) {
        next(new Error('Invalid password'));
    } else {
        next();
    }
});*/

ModelSchema.methods = {

};

mongoose.model('GroupTask', ModelSchema);