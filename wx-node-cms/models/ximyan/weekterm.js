'use strict';

/**
 * 模块依赖
 */
let mongoose = require('mongoose')
let Schema = mongoose.Schema

/**
 * 内容模型
 */
let ModelSchema = new Schema({
    term: {//号周
        type: Number,
        required: true
    },
    start: {// 号周开始时间
        type: Date,
        required: true
    },
    end: {// 号周结束时间
        type: Date,
        required: true
    }
});


ModelSchema.methods = {

};

mongoose.model('Weekterm', ModelSchema);