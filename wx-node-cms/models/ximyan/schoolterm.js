'use strict';

/**
 * 模块依赖
 */
let mongoose = require('mongoose')
let Schema = mongoose.Schema

/**
 * 内容模型
 */
let ModelSchema = new Schema({
    year: {// 年份
        type: String,
        required: true
    },
    term: {// 学期
        type: Number,
        required: true
    },
    start: {// 开始时间
        type: Date,
        required: true
    },
    end: {// 结束时间
        type: Date,
        required: true
    }
});


ModelSchema.methods = {

};

mongoose.model('Schoolterm', ModelSchema);