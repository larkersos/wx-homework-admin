'use strict';

/**
 * 模块依赖
 */
let mongoose = require('mongoose')
let Schema = mongoose.Schema

/**
 * 内容模型
 */
let ModelSchema = new Schema({
    group_class: {//  所属群组班级
        type: Schema.ObjectId,
        ref: 'GroupClass'
    },
    group_user: {// 用户
        type: Schema.ObjectId,
        ref: 'GroupUser'
    },
    teacher: {// 关联认证教师
        type: Schema.ObjectId,
        ref: 'Teacher'
    },
    teacher_type: {// 类型 0 任课老师 1 班主任 2 其他
        type: Number,
        default: 0
    },
    course: {// 所任课程
        type: String,
        required: true
    },
    begin_date: {//开始时间
        type: Date
    },
    end_date: {//结束时间
        type: Date
    },
    created_user: {//创建人
        type: String
    },
    created_source: {//创建来源 wx,sys
        type: String
    },
    created_time: {// 创建时间
        type: Date,
        default: Date.now
    },
    modified_time: {// 修改时间
        type: Date
    },
    status: {//有效状态1 有效 0 无效
        type: Number,
        default: 1
    }
});

/*ContentSchema.pre('save', function(next) {
    if (!this.isNew) return next();
    if (!this.title) {
        next(new Error('Invalid password'));
    } else {
        next();
    }
});*/

ModelSchema.methods = {

};

mongoose.model('GroupTeacher', ModelSchema);