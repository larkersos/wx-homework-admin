'use strict';

/**
 * 模块依赖
 */
let mongoose = require('mongoose');
let Schema = mongoose.Schema;

/**
 * 内容模型
 */
let ModelSchema = new Schema({
    group_task: {//  所属任务
        type: Schema.ObjectId,
        ref: 'GroupTask'
    },
    group_user: {// 用户
        type: Schema.ObjectId,
        ref: 'GroupUser'
    },
    content: {//内容
        type: String,
        required: true
    },
    attachments: [// 附件信息
        {
            type: Schema.ObjectId,
            ref: 'Attachment'
        }
    ],
    parent: {// 关联上级
        type: Schema.ObjectId,
        ref: 'GroupTaskFeedback'
    },
    process_status: {// 处理状态 0未处理 1 已处理 2 处理中
        type: Number
    },
    process_user: {// 处理用户
        type: Schema.ObjectId,
        ref: 'GroupUser'
    },
    process_content: {// 处理意见内容
        type: String
    },
    process_time: {// 处理时间
        type: Date
    },
    begin_date: {//开始时间
        type: Date,
        default: Date.now
    },
    end_date: {//结束时间
        type: Date
    },
    created_user: {//创建人
        type: String
    },
    created_source: {//创建来源 wx,sys
        type: String
    },
    created_time: {// 创建时间
        type: Date,
        default: Date.now
    },
    modified_time: {// 修改时间
        type: Date
    },
    status: {//有效状态1 有效 0 无效
        type: Number,
        default: 1
    }
});

/*ContentSchema.pre('save', function(next) {
    if (!this.isNew) return next();
    if (!this.title) {
        next(new Error('Invalid password'));
    } else {
        next();
    }
});*/

ModelSchema.methods = {

};

mongoose.model('GroupTaskFeedback', ModelSchema);