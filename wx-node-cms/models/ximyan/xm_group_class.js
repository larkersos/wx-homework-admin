'use strict';

/**
 * 模块依赖
 */
let mongoose = require('mongoose')
let Schema = mongoose.Schema

/**
 * 内容模型
 */
let ModelSchema = new Schema({
    class: {//  所属班级
        type: Schema.ObjectId,
        ref: 'Class'
    },
    open_gid: {// 群组id
        type: String,
        required: true
    },
    cover: {// 班级封面logo地址
        type: Schema.ObjectId,
        ref: 'Attachment'
    },
    name: {//名称
        type: String,
        required: true
    },
    master: {// 班主任
        type: Schema.ObjectId,
        ref: 'GroupUser'
    },
    begin_date: {//开始时间
        type: Date
    },
    end_date: {//结束时间
        type: Date
    },
    created_user: {//创建人
        type: String
    },
    created_source: {//创建来源 wx,sys
        type: String
    },
    created_time: {// 创建时间
        type: Date,
        default: Date.now
    },
    modified_time: {// 修改时间
        type: Date
    },
    status: {//有效状态1 有效 0 无效
        type: Number,
        default: 1
    }
});

/*ContentSchema.pre('save', function(next) {
    if (!this.isNew) return next();
    if (!this.title) {
        next(new Error('Invalid password'));
    } else {
        next();
    }
});*/

ModelSchema.methods = {

};

mongoose.model('GroupClass', ModelSchema);