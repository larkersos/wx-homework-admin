'use strict';

/**
 * 模块依赖
 */
let mongoose = require('mongoose')
let Schema = mongoose.Schema

/**
 * 内容模型
 */
let ModelSchema = new Schema({
    group_class: {//  所属群组班级
        type: Schema.ObjectId,
        ref: 'GroupClass'
    },
    union_id: {// 用户唯一码,同一个用户可以有多个角色，比如教师和家长，多个孩子的家长
        type: String,
        required: true
    },
    user_type: {// 用户身份类型：1 教师 2 家长 3 学生
        type: Number
    },
    role_type: {// 角色类型 10 普通老师 11 班主任 21爸爸 22妈妈 23其他 30学生
        type: Number
    },
    name: {//名称
        type: String,
        required: true
    },
    portrait:{ // 头像
        type: Schema.ObjectId,
        ref: 'Attachment'
    }
    begin_date: {//开始时间
        type: Date
    },
    end_date: {//结束时间
        type: Date
    },
    created_user: {//创建人
        type: String
    },
    created_source: {//创建来源 wx,sys
        type: String
    },
    created_time: {// 创建时间
        type: Date,
        default: Date.now
    },
    modified_time: {// 修改时间
        type: Date
    },
    status: {//有效状态1 有效 0 无效
        type: Number,
        default: 1
    }
});

/*ContentSchema.pre('save', function(next) {
    if (!this.isNew) return next();
    if (!this.title) {
        next(new Error('Invalid password'));
    } else {
        next();
    }
});*/

ModelSchema.methods = {

};

mongoose.model('GroupUser', ModelSchema);