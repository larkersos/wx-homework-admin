'use strict';

/**
 * 模块依赖
 */
let mongoose = require('mongoose')
let Schema = mongoose.Schema

/**
 * 内容模型
 */
let ContentSchema = new Schema({
    weekterm: {//号周
        type: Number,
        required: true
    },
    gradeclass: {//班级
        type: Schema.ObjectId,
        ref: 'Gradeclass'
    },
    monday: {
        type: Schema.ObjectId,
        ref: 'Teacher'
    },
    tuesday: {
        type: Schema.ObjectId,
        ref: 'Teacher'
    },
    wednesday: {
        type: Schema.ObjectId,
        ref: 'Teacher'
    },
    thursday: {
        type: Schema.ObjectId,
        ref: 'Teacher'
    },
    friday: {
        type: Schema.ObjectId,
        ref: 'Teacher'
    },
    saturday: {
        type: Schema.ObjectId,
        ref: 'Teacher'
    },
    sunday: {
        type: Schema.ObjectId,
        ref: 'Teacher'
    },
    created: {
        type: Date,
        default: Date.now
    },
    status: {
        type: Number,
        default: 0
    }
});


ContentSchema.methods = {

};

mongoose.model('Morningread', ContentSchema);
