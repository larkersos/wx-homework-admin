'use strict';

/**
 * 模块依赖
 */
let mongoose = require('mongoose')
let Schema = mongoose.Schema

/**
 * 内容模型
 */
let ModelSchema = new Schema({
    school: {//  所属学校
        type: Schema.ObjectId,
        ref: 'School'
    },
    name: {// 姓名
        type: String,
        required: true
    },
    sex: {// 性别1男0 女
        type: Number,
        default: 1
    },
    birthday: {// 出生日期
        type: Date
    },
    begin_date: {//开始时间
        type: Date,
        default: Date.now
    },
    end_date: {//结束时间
        type: Date
    },
    created_user: {//创建人
        type: String
    },
    created_source: {//创建来源 wx,sys
        type: String
    },
    created_time: {// 创建时间
        type: Date,
        default: Date.now
    },
    modified_time: {// 修改时间
        type: Date
    },
    status: {//有效状态1 有效 0 无效
        type: Number,
        default: 1
    }
});


ModelSchema.methods = {

};

mongoose.model('Teacher', ModelSchema);
