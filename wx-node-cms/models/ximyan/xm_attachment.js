'use strict';

/**
 * 模块依赖
 */
let mongoose = require('mongoose');
let Schema = mongoose.Schema;

/**
 * 内容模型
 */
let ModelSchema = new Schema({
    code: {// 编码，对应多个url
        type: String,
        required: true
    },
    url_path: {// 附件地址, 相对地址或网络地址
        type: String,
        required: true
    },
    file_name: {// 文件名
        type: String
    },
    file_suffix: {// 文件后缀名
        type: String
    },
    file_type: {// 类型 image,doc,xls
        type: String
    },
    store_type: {// 类型 0 Local，1 Net
        type: Number,
        default: 1
    },
    created_user: {//创建人
        type: String
    },
    created_source: {//创建来源 wx,sys
        type: String
    },
    created_time: {// 创建时间
        type: Date,
        default: Date.now
    },
    modified_time: {// 修改时间
        type: Date
    },
    status: {//有效状态1 有效 0 无效
        type: Number,
        default: 1
    }
});


ModelSchema.methods = {

};

mongoose.model('Attachment', ModelSchema);
