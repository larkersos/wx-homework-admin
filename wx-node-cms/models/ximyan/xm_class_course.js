'use strict';

/**
 * 模块依赖
 */
let mongoose = require('mongoose')
let Schema = mongoose.Schema

/**
 * 内容模型
 */
let ModelSchema = new Schema({
    class: {//  所属班级
        type: Schema.ObjectId,
        ref: 'Class'
    },
    term: {// 学期
        type: Number,
        default: 1
    },
    week: {// 计算所得周
        type: Number,
        default: 1
    },
    day: {// 星期几1-7
        type: Number,
        default: 1
    },
    phase: {// 第几节，0为早自习
        type: Number,
        default: 0
    },
    course: {// 课程
        type: String
    },
    begin_date: {//开始时间
        type: Date
    },
    end_date: {//结束时间
        type: Date
    },
    created_user: {//创建人
        type: String
    },
    created_source: {//创建来源 wx,sys
        type: String
    },
    created_time: {// 创建时间
        type: Date,
        default: Date.now
    },
    modified_time: {// 修改时间
        type: Date
    },
    status: {//有效状态1 有效 0 无效
        type: Number,
        default: 1
    }
});

/*ContentSchema.pre('save', function(next) {
    if (!this.isNew) return next();
    if (!this.title) {
        next(new Error('Invalid password'));
    } else {
        next();
    }
});*/

ModelSchema.methods = {

};

mongoose.model('ClassCourse', ModelSchema);