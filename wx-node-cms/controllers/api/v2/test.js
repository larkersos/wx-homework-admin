'use strict';

let mongoose = require('mongoose')
let core = require('../../../libs/core')
let testService = require('../../../services/test')
let _ = require('lodash');

exports.showTeacher = async function(req, res) {
    let id = req.params.id;
    let data = null
    let error
    try {
        data = await testService.findById(id)
    } catch (e) {
        // error = e.message
        console.error(e)
        error = '系统异常'
    }
    if (data == null)
        data = "这是一个NULL"
    
    res.json({
        success: !error,
        data: data,
        error: error
    });
}

exports.create = async function(req, res) {
    let obj = req.body
    // let obj = _.pick(req.body, 'name', 'age', 'sex');
    // TODO： 校验输入
    // 后台创建用户
    let user = req.user
    if (user) {
        obj.author = mongoose.Types.ObjectId(user._id)
    }

    let data = null
    let error
    try {
        data = await testService.create(obj)
    } catch (e) {
        error = e.message
        // error = '系统异常'
    }
    res.json({
        success: !error,
        data: data,
        error: error
    })
}

exports.update = async function(req, res) {
    let id = req.params.id
    // let obj = req.body
    let obj = _.pick(req.body, 'url', 'md_url', 'sm_url', 'size', 'type', 'description');
    // TODO： 校验输入
    let data = null
    let error
    try {
        let isAdmin = req.Roles && req.Roles.indexOf('admin') > -1;
        let item = await fileService.findById(id)
        let isAuthor = !!(item.author && ((item.author + '') === (req.user._id + '')))
        if(!isAdmin && !isAuthor) {
            error = '没有权限'
        } else {
            data = await testService.findByIdAndUpdate(id, obj, {new: true})
        }
    } catch (e) {
        // error = e.message
        error = '系统异常'
    }
    res.json({
        success: !error,
        data: data,
        error: error
    })
}

exports.destroy = async function(req, res) {
    let id = req.params.id
    let data = null
    let error
    try {
        let isAdmin = req.Roles && req.Roles.indexOf('admin') > -1;
        let item = await fileService.findById(id)
        let isAuthor = !!(item.author && ((item.author + '') === (req.user._id + '')))
        if(!isAdmin && !isAuthor) {
            error = '没有权限'
        } else {
            data = testService.findByIdAndRemove(id)
        }
        
    } catch (e) {
        // error = e.message
        error = '系统异常'
    }
    res.json({
        success: !error,
        data: data,
        error: error
    })
}