'use strict';

let mongoose = require('mongoose')
let Weekterm = mongoose.model('Weekterm')
let _ = require('lodash')
let core = require('../../../libs/core')
let nodeExcel = require('excel-export')//关联excel-export模块
let moment = require('moment')//关联moment模块

//列表
exports.list = function(req, res) {
    let condition = {};
    let conditionWeekterm = {};
    let date = "";
    if (req.method === 'GET') {
        date = req.query.date;
    }else{
        date = req.body.date;
    }
    console.log("===============date="+ date);
    if(date) {
        condition.date = date;
        conditionWeekterm = {"$and":[{"start":{"$lte":date}},{"end":{"$gte":date}}]};
    }
    console.log("===============conditionWeekterm="+JSON.stringify(conditionWeekterm));

    //查数据总数
    Weekterm.count(conditionWeekterm).exec().then(function(total) {
        let query = Weekterm.find(conditionWeekterm);
        //分页
        let pageInfo = core.createPage(req.query.page, total);
        //console.log(pageInfo);
        query.skip(pageInfo.start);
        query.limit(pageInfo.pageSize);
        query.sort({term: 1,start:1});
        query.exec(function(err, results) {
            //console.log(err, results);
            // console.log("==results="+ JSON.stringify(results));
            res.render('server/ximyan/weekterm/list', {
                title: '号周列表',
                results: results,
                pageInfo: pageInfo,
                condition:condition,
                Menu: 'list'
            });
        });
    });
};
//列表
exports.exportData  = function(req, res) {
    let condition = {};
    let conditionWeekterm = {};
    let date = "";
    if (req.method === 'GET') {
        date = req.query.date;
    }else{
        date = req.body.date;
    }
    console.log("===============date="+date);
    if(date) {
        condition.date = date;
        conditionWeekterm = {"$and":[{"start":{"$lte":date}},{"end":{"$gte":date}}]};
    }

    //查数据总数

    let query = Weekterm.find(conditionWeekterm);
    //console.log(pageInfo);
    query.skip(0);
    query.limit(99999);
    query.sort({term: 1,start:1});
    query.exec(function(err, results) {

        var conf = {};
        conf.cols = [
            {caption:'序号', type:'number'},
            {caption:'号周', type:'number'},
            {caption:'开始时间', type:'string', width: 800},
            {caption:'结束时间', type:'string', width:800},
        ];

        // 以下为将数据封装成array数组。因为下面的方法里头只接受数组。
        var vac = new Array();
        for (var i = 0; i < results.length; i++) {
            var temp = new Array();
            temp[0]= (i+1);
            temp[1]= results[i].term;
            temp[2]= moment(results[i].start).format("YYYY-MM-DD") //格式串moment + "";
            temp[3] = moment(results[i].end).format("YYYY-MM-DD") //格式串moment + "";
            vac.push(temp);
        };
        console.log("===vac="+JSON.stringify(vac));
        conf.rows = vac;//conf.rows只接受数组
        var result = nodeExcel.execute(conf);
        res.setHeader('Content-Type', 'application/vnd.openxmlformats');
        res.setHeader("Content-Disposition", "attachment; filename=exportReport.xlsx");
        res.end(result, 'binary');
    });
};
//添加
exports.add = function(req, res) {
    if (req.method === 'GET') {
        let condition = {};
        res.render('server/ximyan/weekterm/add', {
            Menu: 'add'
        });
    } else if (req.method === 'POST') {
        let obj = _.pick(req.body, 'term', 'start', 'end');

        let weekterm = new Weekterm(obj);
        weekterm.save(function(err, content) {
            console.log("err="+err);
            if (req.xhr) {
                return res.json({
                    status: !err
                })
            }
            if (err) {
                console.log(err);
                return res.render('server/info', {
                    message: '创建失败'
                });
            }

            res.render('server/info', {
                message: '创建成功'
            });
        });
    }
};
exports.edit = function(req, res) {
    if(req.method === 'GET') {
        let id = req.params.id;
        Weekterm.findById(id).exec(function(err, result) {
            if(err) {
                console.log('加载内容失败');
            }
            let isAdmin = req.Roles && req.Roles.indexOf('admin') > -1;
            if(!isAdmin) {
                return res.render('server/info', {
                    message: '没有权限'
                });
            }
            //console.log(tags)
            res.render('server/ximyan/weekterm/edit', {
                result: result,
                Menu: 'edit'
            });
        });
    } else if(req.method === 'POST') {
        let id = req.params.id;
        let obj = _.pick(req.body, 'term', 'start', 'end');
        console.log(obj);

        Weekterm.findById(id).exec(function(err, result) {
            //console.log(result);
            let isAdmin = req.Roles && req.Roles.indexOf('admin') > -1;

            if(!isAdmin) {
                return res.render('server/info', {
                    message: '没有权限'
                });
            }
            _.assign(result, obj);
            result.save(function(err, weekterm) {
                if (req.xhr) {
                    return res.json({
                        status: !err
                    })
                }
                if(err || !weekterm) {
                    return res.render('server/info', {
                        message: '修改失败'
                    });
                }
                res.render('server/info', {
                    message: '更新成功'
                });
            });
        });
    }
};
//删除
exports.del = function(req, res) {
    let id = req.params.id;
    Weekterm.findById(id).exec(function(err, result) {
        if(err || !result) {
            return res.render('server/info', {
                message: '内容不存在'
            });
        }
        let isAdmin = req.Roles && req.Roles.indexOf('admin') > -1;

        if(!isAdmin ) {
            return res.render('server/info', {
                message: '没有权限'
            });
        }
        //
        result.remove(function(err) {
            if (req.xhr) {
                return res.json({
                    status: !err
                });
            }
            if(err) {
                return res.render('server/info', {
                    message: '删除失败'
                });
            }
            res.render('server/info', {
                message: '删除成功'
            })
        });
    });
};
