'use strict';

let mongoose = require('mongoose')
let Teacher = mongoose.model('Teacher')
let _ = require('lodash')
let core = require('../../../libs/core')
let nodeExcel = require('excel-export')//关联excel-export模块
let moment = require('moment')//关联moment模块

//列表
exports.list = function(req, res) {
    let condition = {};
    let username = "";
    let course = "";
    let year = "";
    if (req.method === 'GET') {
        course = req.query.course;
        year = req.query.year;
        condition.username = username;
        condition.year = year;
        condition.course = course;
    }else{
        course = req.body.course;
        username = req.body.username;
        year = req.body.year;
        condition.username = username;
        condition.year = year;
        condition.course = course;
    }
    console.log("===============condition="+ JSON.stringify(condition));

    //查数据总数
    Teacher.count(condition).exec().then(function(total) {
        let query = Teacher.find(conditionTeacher);
        //分页
        let pageInfo = core.createPage(req.query.page, total);
        //console.log(pageInfo);
        query.skip(pageInfo.start);
        query.limit(pageInfo.pageSize);
        query.sort({cource: 1});
        query.exec(function(err, results) {
            //console.log(err, results);
            // console.log("==results="+ JSON.stringify(results));
            res.render('server/teacher/list', {
                title: '号周列表',
                results: results,
                pageInfo: pageInfo,
                condition:condition,
                Menu: 'list'
            });
        });
    });
};
//列表
exports.exportData  = function(req, res) {
    let condition = {};
    let username = "";
    let course = "";
    let year = "";
    if (req.method === 'GET') {
        course = req.query.course;
        year = req.query.year;
        condition.username = username;
        condition.year = year;
        condition.course = course;
    }else{
        course = req.body.course;
        username = req.body.username;
        year = req.body.year;
        condition.username = username;
        condition.year = year;
        condition.course = course;
    }
    console.log("===============condition="+ JSON.stringify(condition));

    //查数据总数
    let query = Teacher.find(condition);
    //console.log(pageInfo);
    query.skip(0);
    query.limit(99999);
    query.sort({year: 1,course:1});
    query.exec(function(err, results) {
        var conf = {};
        conf.cols = [
            {caption:'序号', type:'string', width: 10},
            {caption:'年份', type:'string', width: 300},
            {caption:'姓名', type:'string', width: 300},
            {caption:'课程', type:'string', width: 800},
        ];

        // 以下为将数据封装成array数组。因为下面的方法里头只接受数组。
        var vac = new Array();
        for (var i = 0; i < results.length; i++) {
            var temp = new Array();
            temp[0]= i+1+"";
            temp[1]= results[i].year;
            temp[2]= results[i].person.username;
            temp[3]= results[i].course;
            vac.push(temp);
        };
        conf.rows = vac;//conf.rows只接受数组
        var result = nodeExcel.execute(conf);
        res.setHeader('Content-Type', 'application/vnd.openxmlformats');
        res.setHeader("Content-Disposition", "attachment; filename=exportReport.xlsx");
        res.end(result, 'binary');
    });
};
//添加
exports.add = function(req, res) {
    if (req.method === 'GET') {
        let condition = {};
        res.render('server/teacher/add', {
            Menu: 'add'
        });
    } else if (req.method === 'POST') {
        let obj = _.pick(req.body, 'person', 'course', 'year');

        let teacher = new Teacher(obj);
        Teacher.save(function(err, teacher) {
            console.log("err="+err);
            if (req.xhr) {
                return res.json({
                    status: !err
                })
            }
            if (err) {
                console.log(err);
                return res.render('server/info', {
                    message: '创建失败'
                });
            }

            res.render('server/info', {
                message: '创建成功'
            });
        });
    }
};
exports.edit = function(req, res) {
    if(req.method === 'GET') {
        let id = req.params.id;
        Teacher.findById(id).exec(function(err, result) {
            if(err) {
                console.log('加载内容失败');
            }
            let isAdmin = req.Roles && req.Roles.indexOf('admin') > -1;
            if(!isAdmin) {
                return res.render('server/info', {
                    message: '没有权限'
                });
            }
            //console.log(tags)
            res.render('server/teacher/edit', {
                result: result,
                Menu: 'edit'
            });
        });
    } else if(req.method === 'POST') {
        let id = req.params.id;
        let obj = _.pick(req.body, 'person', 'course', 'year');
        console.log(obj);

        Teacher.findById(id).exec(function(err, result) {
            //console.log(result);
            let isAdmin = req.Roles && req.Roles.indexOf('admin') > -1;

            if(!isAdmin) {
                return res.render('server/info', {
                    message: '没有权限'
                });
            }
            _.assign(result, obj);
            result.save(function(err, teacher) {
                if (req.xhr) {
                    return res.json({
                        status: !err
                    })
                }
                if(err || !teacher) {
                    return res.render('server/info', {
                        message: '修改失败'
                    });
                }
                res.render('server/info', {
                    message: '更新成功'
                });
            });
        });
    }
};
//删除
exports.del = function(req, res) {
    let id = req.params.id;
    Teacher.findById(id).exec(function(err, result) {
        if(err || !result) {
            return res.render('server/info', {
                message: '内容不存在'
            });
        }
        let isAdmin = req.Roles && req.Roles.indexOf('admin') > -1;

        if(!isAdmin ) {
            return res.render('server/info', {
                message: '没有权限'
            });
        }
        //
        result.remove(function(err) {
            if (req.xhr) {
                return res.json({
                    status: !err
                });
            }
            if(err) {
                return res.render('server/info', {
                    message: '删除失败'
                });
            }
            res.render('server/info', {
                message: '删除成功'
            })
        });
    });
};
