'use strict';

let mongoose = require('mongoose')
let MorningRead = mongoose.model('Morningread')
let _ = require('lodash')
let core = require('../../../libs/core')
let nodeExcel = require('excel-export')//关联excel-export模块
let moment = require('moment')//关联moment模块\

//列表
exports.list = function(req, res) {
    let condition = {};
    let schoolYear = "";
    let term = "";
    let weekday ="";
    if (req.method === 'GET') {
        schoolYear = req.query.schoolYear;
        term = req.query.term;
        weekday = req.query.weekday;
    }else{
        schoolYear = req.body.schoolYear;
        term = req.body.term;
        weekday = req.body.weekday;
    }
    if(schoolYear) {
        condition.schoolYear = category;
    }
    if(term) {
        condition.term = term;
    }
    if(weekday) {
        condition.weekday = weekday;
    }
    console.log("===============schoolYear="+schoolYear+"，term="+term);

    //查数据总数
    MorningRead.count(condition).exec().then(function(total) {
        let query = MorningRead.find(condition).populate('teacher', 'name course class schoolYear term grade');
        //分页
        let pageInfo = core.createPage(req.query.page, total);
        //console.log(pageInfo);
        query.skip(pageInfo.start);
        query.limit(pageInfo.pageSize);
        query.sort({weekday: -1});
        query.exec(function(err, results) {
            //console.log(err, results);
            // console.log("==results="+ JSON.stringify(results));
            res.render('server/ximyan/morningread/list', {
                title: '早读列表',
                contents: results,
                pageInfo: pageInfo,
                condition:condition,
                Menu: 'list'
            });
        });
    });
};
//列表
exports.exportData  = function(req, res) {
    let condition = {};
    let category = "";
    let title = "";
    if (req.method === 'GET') {
        category = req.query.category;
        title = req.query.title;
    }else{
        category = req.body.category;
        title = req.body.title;
    }
    console.log("===exportData========category="+category+"，title="+title);
    if(category) {
        condition.category = category;
    }
    if(title) {
        var pattern = new RegExp("^.*"+title+".*$");
        condition.title = pattern;
    }

    if(req.Roles && req.Roles.indexOf('admin') < 0) {
        condition.author = req.session.user._id;
    }

    let conditionCategorys = {};
    if(req.Roles && req.Roles.indexOf('admin') < 0) {
        conditionCategorys.author = req.session.user._id;
    }

    let categoryslist=[];
    Category.find(conditionCategorys, function(err, categorys) {
        categoryslist = categorys;
    })

    //查数据总数
    let query = Content.find(condition).populate('author', 'username name email');
    //console.log(pageInfo);
    query.skip(0);
    query.limit(99999);
    query.sort({created: -1});
    query.exec(function(err, results) {
        var conf = {};
        conf.cols = [
            {caption:'序号', type:'string', width: 10},
            {caption:'标题', type:'string', width: 300},
            {caption:'作者', type:'string', width: 100},
            {caption:'发布时间', type:'string', width: 100},
        ];

        // 以下为将数据封装成array数组。因为下面的方法里头只接受数组。
        var vac = new Array();
        for (var i = 0; i < results.length; i++) {
            var temp = new Array();
            temp[0]= i+1+"";
            temp[1]= results[i].title;
            temp[2]= results[i].author.name;
            temp[3] = moment(results[i].created).format("YYYY-MM-DD HH:mm:ss") //格式串moment + "";
            vac.push(temp);
        };
        conf.rows = vac;//conf.rows只接受数组
        var result = nodeExcel.execute(conf);
        res.setHeader('Content-Type', 'application/vnd.openxmlformats');
        res.setHeader("Content-Disposition", "attachment; filename=exportReport.xlsx");
        res.end(result, 'binary');
    });
};
//添加
exports.add = function(req, res) {
    if (req.method === 'GET') {
        let condition = {};
        if(req.Roles && req.Roles.indexOf('admin') < 0) {
            condition.author = req.session.user._id;
        }
        Promise.all([Teacher.find(condition).exec()]).then((result) => {
            res.render('server/ximyan/morningread/add', {
                teacher: result[0],
                Menu: 'add'
            });
        }).catch((e) => {
            res.render('server/ximyan/morningread/add', {
                Menu: 'add'
            });
        })
    } else if (req.method === 'POST') {
        let obj = _.pick(req.body, 'title', 'summary', 'content', 'gallery', 'category', 'tags');
        if (req.session.user) {
            obj.author = req.session.user._id;
        }

        let moringreadModdel = new MorningRead(obj);
        moringreadModdel.save(function(err, moringreadModdel) {
            if (req.xhr) {
                return res.json({
                    status: !err
                })
            }
            if (err) {
                console.log(err);
                return res.render('server/info', {
                    message: '创建失败'
                });
            }

            res.render('server/info', {
                message: '创建成功'
            });
        });
    }
};
exports.edit = function(req, res) {
    if(req.method === 'GET') {
        let id = req.params.id;
        Content.findById(id).populate('author gallery tags').exec(function(err, result) {
            if(err) {
                console.log('加载内容失败');
            }
            let isAdmin = req.Roles && req.Roles.indexOf('admin') > -1;
            let isAuthor = result.author && ((result.author._id + '') === req.session.user._id);

            if(!isAdmin && !isAuthor) {
                return res.render('server/info', {
                    message: '没有权限'
                });
            }
            let condition = {};
            if(req.Roles && req.Roles.indexOf('admin') < 0) {
                condition.author = req.session.user._id;
            }
            Category.find(condition, function(err, categorys) {
                Tag.find(condition).exec().then(function(tags) {
                    //console.log(tags)
                    res.render('server/content/edit', {
                        content: result,
                        categorys: categorys,
                        tags: tags,
                        Menu: 'edit'
                    });
                });
                
            });
        });
    } else if(req.method === 'POST') {
        let id = req.params.id;
        let obj = _.pick(req.body, 'title', 'summary', 'content', 'gallery', 'category', 'tags');
        console.log(obj);
        console.log(obj.gallery)
        if(obj.category === '') {
            obj.category = null;
        }
        if(!obj.gallery) {
            obj.gallery = [];
        }
        
        Content.findById(id).populate('author').exec(function(err, result) {
            //console.log(result);
            let isAdmin = req.Roles && req.Roles.indexOf('admin') > -1;
            let isAuthor = result.author && ((result.author._id + '') === req.session.user._id);

            if(!isAdmin && !isAuthor) {
                return res.render('server/info', {
                    message: '没有权限'
                });
            }
            _.assign(result, obj);
            result.save(function(err, content) {
                if (req.xhr) {
                    return res.json({
                        status: !err
                    })
                }
                if(err || !content) {
                    return res.render('server/info', {
                        message: '修改失败'
                    });
                }
                res.render('server/info', {
                    message: '更新成功'
                });
            });
        });
    }
};
//删除
exports.del = function(req, res) {
    let id = req.params.id;
    Content.findById(id).populate('author').exec(function(err, result) {
        if(err || !result) {
            return res.render('server/info', {
                message: '内容不存在'
            });
        }
        let isAdmin = req.Roles && req.Roles.indexOf('admin') > -1;
        let isAuthor = result.author && ((result.author._id + '') === req.session.user._id);

        if(!isAdmin && !isAuthor) {
            return res.render('server/info', {
                message: '没有权限'
            });
        }
        //
        result.remove(function(err) {
            if (req.xhr) {
                return res.json({
                    status: !err
                });
            }
            if(err) {
                return res.render('server/info', {
                    message: '删除失败'
                });
            }
            res.render('server/info', {
                message: '删除成功'
            })
        });
    });
};
