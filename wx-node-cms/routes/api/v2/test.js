'use strict';

let express = require('express')
let router = express.Router()
let test = require('../../../controllers/api/v2/test')
//
router.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*')
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE')
    res.setHeader('Access-Control-Allow-Headers', 'X-Request-With,content-type,Authorization')
    next();
});

router.route('/teacher/:id')
    .get(test.showTeacher)

router.route('/teacher')
    .post(test.create)

module.exports = function(app) {
    app.use('/api/v2/test', router);
};
