'use strict';


let express = require('express')
let router = express.Router()
let core = require('../../../libs/core')
let action = require('../../../middlewares/action')
let controllerServer = require('../../../controllers/server/ximyan/weekterm')

//权限判断
router.use(function(req, res, next) {
    console.log('号周页: ' + Date.now());
    res.locals.Path = 'weekterm';
    if(!req.session.user) {
        let path = core.translateAdminDir('/user/login');
        return res.redirect(path);
    }
    next();
});

//内容列表
router.route('/').get(action.checkAction('WEEKTERM_INDEX'), controllerServer.list);
router.route('/list').get(action.checkAction('WEEKTERM_INDEX'), controllerServer.list);
router.route('/list').post(action.checkAction('WEEKTERM_INDEX'), controllerServer.list);
router.route('/exportData').post(action.checkAction('WEEKTERM_INDEX'), controllerServer.exportData);
//添加内容
router.route('/add').all(action.checkAction('WEEKTERM_CREATE'), controllerServer.add);
//更新信息
router.route('/:id/edit').all(action.checkAction('WEEKTERM_UPDATE'), controllerServer.edit);
//删除信息
router.route('/:id/del').all(action.checkAction('WEEKTERM_DELETE'), controllerServer.del);


module.exports = function(app) {
    let path = core.translateAdminDir('/ximyan/weekterm');
    app.use(path, router);
};
