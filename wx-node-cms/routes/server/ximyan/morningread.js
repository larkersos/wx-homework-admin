'use strict';

let express = require('express');
let router = express.Router();
let core = require('../../../libs/core');
let action = require('../../../middlewares/action');
let controllerServer = require('../../../controllers/server/ximyan/morningread');

//权限判断
router.use(function(req, res, next) {
    console.log('早读管理页: ' + Date.now());
    res.locals.Path = 'morningread';
    if(!req.session.user) {
        let path = core.translateAdminDir('/user/login');
        return res.redirect(path);
    }
    next();
});

//内容列表
router.route('/').get(action.checkAction('MORNINGREAD_INDEX'), controllerServer.list);
router.route('/list').get(action.checkAction('MORNINGREAD_INDEX'), controllerServer.list);
router.route('/list').post(action.checkAction('MORNINGREAD_INDEX'), controllerServer.list);
router.route('/exportData').post(action.checkAction('MORNINGREAD_INDEX'), controllerServer.exportData);
//添加内容
router.route('/add').all(action.checkAction('MORNINGREAD_CREATE'), controllerServer.add);
//更新信息
router.route('/:id/edit').all(action.checkAction('MORNINGREAD_UPDATE'), controllerServer.edit);
//删除信息
router.route('/:id/del').all(action.checkAction('MORNINGREAD_DELETE'), controllerServer.del);


module.exports = function(app) {
    let path = core.translateAdminDir('/ximyan/morningread');
    app.use(path, router);
};

