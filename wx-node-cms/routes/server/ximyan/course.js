'use strict';

let path = require('path');
let rootPath = path.join(__dirname,"../../../");

let express = require('express');
let router = express.Router();
let core = require(path.join(rootPath,'libs/core'));
let action = require(path.join(rootPath,'middlewares/action'));
let course = require(path.join(rootPath,'controllers/server/ximyan/course'));

//权限判断
router.use(function(req, res, next) {
    console.log('课程管理页: ' + Date.now());
    res.locals.Path = 'course';
    if(!req.session.user) {
        let path = core.translateAdminDir('/user/login');
        return res.redirect(path);
    }
    next();
});

//内容列表
router.route('/').get(action.checkAction('COURSE_INDEX'), course.list);
router.route('/list').get(action.checkAction('COURSE_INDEX'), course.list);
router.route('/list').post(action.checkAction('COURSE_INDEX'), course.list);
router.route('/exportData').post(action.checkAction('COURSE_INDEX'), course.exportData);
//添加内容
router.route('/add').all(action.checkAction('COURSE_CREATE'), course.add);
//更新信息
router.route('/:id/edit').all(action.checkAction('COURSE_UPDATE'), course.edit);
//删除信息
router.route('/:id/del').all(action.checkAction('COURSE_DELETE'), course.del);


module.exports = function(app) {
    let path = core.translateAdminDir('/ximyan/course');
    app.use(path, router);
};
