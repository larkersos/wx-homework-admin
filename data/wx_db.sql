/*
SQLyog Ultimate v12.08 (64 bit)
MySQL - 5.6.25-log : Database - xm_wx
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`xm_wx` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `xm_wx`;

/*Table structure for table `wx_attachment` */

DROP TABLE IF EXISTS `wx_attachment`;

CREATE TABLE `wx_attachment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `code` varchar(20) NOT NULL COMMENT '编码，对应多个url',
  `url_path` varchar(100) NOT NULL COMMENT '媒体地址, 相对地址或网络地址',
  `file_name` varchar(100) DEFAULT NULL COMMENT '文件名',
  `file_suffix` varchar(20) DEFAULT NULL COMMENT '文件后缀名',
  `file_type` int(5) DEFAULT NULL COMMENT '类型 image,doc,xls',
  `store_type` int(5) DEFAULT NULL COMMENT '类型 0 Local，1 Net',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modified_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `yn` int(1) DEFAULT '1' COMMENT '是否已删除 1:未删除 0:已删除',
  PRIMARY KEY (`id`),
  KEY `idx_code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='附件信息';

/*Table structure for table `wx_class` */

DROP TABLE IF EXISTS `wx_class`;

CREATE TABLE `wx_class` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `school_id` int(11) DEFAULT NULL COMMENT 'school',
  `year` varchar(4) NOT NULL DEFAULT '2018' COMMENT '入学年份',
  `serial_no` int(2) DEFAULT NULL COMMENT '班级序号',
  `code` varchar(20) NOT NULL DEFAULT '1' COMMENT '班级编码',
  `name` varchar(50) DEFAULT NULL COMMENT '名称',
  `master` int(11) DEFAULT NULL COMMENT '班主任id',
  `begin_date` datetime DEFAULT NULL COMMENT '开始时间',
  `end_date` datetime DEFAULT NULL COMMENT '结束时间',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '有效状态1 有效 0 无效',
  `created_user` varchar(50) NOT NULL COMMENT '创建人',
  `created_source` varchar(20) NOT NULL COMMENT '创建来源 wx,sys',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modified_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='班级信息';

/*Table structure for table `wx_class_course` */

DROP TABLE IF EXISTS `wx_class_course`;

CREATE TABLE `wx_class_course` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `class_id` int(11) NOT NULL COMMENT '班级id',
  `class_name` varchar(50) DEFAULT NULL COMMENT '班级名,冗余字段',
  `term` varchar(20) NOT NULL DEFAULT '0' COMMENT '学期',
  `week` int(2) NOT NULL COMMENT '计算所得周',
  `day` int(1) NOT NULL COMMENT '星期几1-7',
  `phase` int(2) NOT NULL COMMENT '第几节，0为早自习',
  `course` varchar(50) NOT NULL COMMENT '课程',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modified_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='班级课程表';

/*Table structure for table `wx_class_teacher` */

DROP TABLE IF EXISTS `wx_class_teacher`;

CREATE TABLE `wx_class_teacher` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `class_id` int(11) NOT NULL COMMENT '班级id',
  `course` varchar(20) NOT NULL DEFAULT '0' COMMENT '课程名称',
  `teacher_id` int(11) NOT NULL COMMENT '教师id',
  `name` varchar(50) DEFAULT NULL COMMENT '教师姓名，冗余',
  `begin_date` datetime DEFAULT NULL COMMENT '开始时间',
  `end_date` datetime DEFAULT NULL COMMENT '结束时间',
  `status` int(1) DEFAULT '1' COMMENT '有效状态1有效，0无效',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modified_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='班级任课教师';

/*Table structure for table `wx_group_class` */

DROP TABLE IF EXISTS `wx_group_class`;

CREATE TABLE `wx_group_class` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `class_id` int(11) NOT NULL COMMENT '关联班级id',
  `open_gid` varchar(20) NOT NULL COMMENT '群组id',
  `name` varchar(50) NOT NULL COMMENT '班级名称',
  `icon` varchar(100) DEFAULT NULL COMMENT '班级封面logo地址',
  `created_user` varchar(50) DEFAULT NULL COMMENT '创建人-用户唯一编码',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_user` varchar(50) DEFAULT NULL COMMENT '更新人',
  `modified_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `last_update_no` int(10) NOT NULL DEFAULT '1' COMMENT '最后更新编号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='群组班级';

/*Table structure for table `wx_group_teacher` */

DROP TABLE IF EXISTS `wx_group_teacher`;

CREATE TABLE `wx_group_teacher` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `group_class_id` bigint(11) NOT NULL COMMENT '群组班级id',
  `union_id` varchar(50) NOT NULL COMMENT '用户唯一码',
  `course` varchar(50) NOT NULL COMMENT '所任课程',
  `type` int(5) NOT NULL DEFAULT '0' COMMENT '类型 0 任课老师 1 班主任 2 其他',
  `teacher_id` int(11) DEFAULT NULL COMMENT '教师id',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='群组教师表';

/*Table structure for table `wx_group_user` */

DROP TABLE IF EXISTS `wx_group_user`;

CREATE TABLE `wx_group_user` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `group_class_id` varchar(50) NOT NULL COMMENT '群组班级id',
  `union_id` varchar(50) NOT NULL COMMENT '用户唯一码,同一个用户可以有多个角色，比如教师和家长，多个孩子的家长',
  `user_type` int(5) NOT NULL COMMENT '用户身份类型：1 教师 2 家长 3 学生',
  `role_type` int(5) NOT NULL COMMENT '角色类型 10 普通老师 11 班主任 21爸爸 22妈妈 23其他 30学生',
  `name` varchar(50) NOT NULL COMMENT '用户名称',
  `remark` varchar(100) DEFAULT NULL COMMENT '备注',
  `status` int(1) DEFAULT '1' COMMENT '状态 1有效 0 已退出',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modified_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `yn` int(1) NOT NULL DEFAULT '1' COMMENT '是否已删除 1:未删除 0:已删除',
  PRIMARY KEY (`id`,`union_id`),
  KEY `idx_union` (`union_id`),
  KEY `idx_groupClass` (`group_class_id`),
  KEY `idx_groupUser` (`group_class_id`,`user_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='群组用户表';

/*Table structure for table `wx_homework` */

DROP TABLE IF EXISTS `wx_homework`;

CREATE TABLE `wx_homework` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `group_class_id` bigint(11) NOT NULL COMMENT '群组班级id',
  `user_id` bigint(11) DEFAULT NULL COMMENT '发布用户id',
  `type` int(5) DEFAULT '1' COMMENT '类型 1作业 2公告通知',
  `title` varchar(50) DEFAULT NULL COMMENT '作业标题',
  `content` varchar(1000) DEFAULT NULL COMMENT '作业内容',
  `media_code` varchar(20) DEFAULT NULL COMMENT '多媒体编码，图片视频等',
  `deadline` timestamp NULL DEFAULT NULL COMMENT '截止时间',
  `feedback_type` int(5) DEFAULT '0' COMMENT '反馈类型 0 不需要 1 需要确认 2 需要回复',
  `status` int(1) DEFAULT '1' COMMENT '状态 1有效 0 失效',
  `created_user` varchar(50) DEFAULT NULL COMMENT '创建人',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modified_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `yn` int(1) DEFAULT '1' COMMENT '是否已删除 1:未删除 0:已删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='作业任务';

/*Table structure for table `wx_homework_feedback` */

DROP TABLE IF EXISTS `wx_homework_feedback`;

CREATE TABLE `wx_homework_feedback` (
  `id` bigint(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `homework_id` bigint(11) NOT NULL COMMENT '作业任务id',
  `user_id` bigint(11) NOT NULL COMMENT '反馈人id',
  `feedback_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '反馈时间',
  `content` varchar(1000) DEFAULT NULL COMMENT '反馈内容',
  `media_code` varchar(20) DEFAULT NULL COMMENT '反馈多媒体编码，图片视频等',
  `process_user_id` int(11) DEFAULT NULL COMMENT '处理人',
  `process_status` int(5) NOT NULL DEFAULT '0' COMMENT '处理状态 0未处理 1 已处理 2 处理中',
  `process_content` varchar(100) DEFAULT NULL COMMENT '处理意见内容',
  `process_time` timestamp NULL DEFAULT NULL COMMENT '处理时间',
  `parent_id` int(11) DEFAULT NULL COMMENT '关联上级id',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modified_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `yn` int(1) DEFAULT '1' COMMENT '是否已删除 1:未删除 0:已删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `wx_media` */

DROP TABLE IF EXISTS `wx_media`;

CREATE TABLE `wx_media` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `code` varchar(20) NOT NULL COMMENT '编码，对应多个url',
  `url_path` varchar(100) DEFAULT NULL COMMENT '媒体地址, 相对地址或网络地址',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modified_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `yn` int(1) DEFAULT '1' COMMENT '是否已删除 1:未删除 0:已删除',
  PRIMARY KEY (`id`),
  KEY `idx_code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='媒体信息';

/*Table structure for table `wx_school` */

DROP TABLE IF EXISTS `wx_school`;

CREATE TABLE `wx_school` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `city` varchar(50) DEFAULT NULL COMMENT '城市名称',
  `name` varchar(50) DEFAULT NULL COMMENT '名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `wx_teacher` */

DROP TABLE IF EXISTS `wx_teacher`;

CREATE TABLE `wx_teacher` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `school_id` int(11) DEFAULT NULL COMMENT 'school',
  `name` varchar(50) NOT NULL COMMENT '姓名',
  `sex` int(1) NOT NULL DEFAULT '1' COMMENT '性别1男0 女',
  `birthday` datetime DEFAULT NULL COMMENT '出生日期',
  `begin_date` datetime DEFAULT NULL COMMENT '开始时间',
  `end_date` datetime DEFAULT NULL COMMENT '结束时间',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '有效状态1 有效 0 无效',
  `created_user` varchar(50) NOT NULL COMMENT '创建人',
  `created_source` varchar(20) NOT NULL COMMENT '创建来源 wx,sys',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modified_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='教师信息';

/*Table structure for table `wx_term` */

DROP TABLE IF EXISTS `wx_term`;

CREATE TABLE `wx_term` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `school_id` int(11) DEFAULT NULL COMMENT 'school',
  `name` varchar(50) DEFAULT NULL COMMENT '名称',
  `year` int(4) NOT NULL DEFAULT '2018' COMMENT '学年',
  `term` int(2) NOT NULL DEFAULT '1' COMMENT '学期',
  `begin_date` datetime NOT NULL COMMENT '开始时间',
  `end_date` datetime NOT NULL COMMENT '结束时间',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `modified_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='学期时间';

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
